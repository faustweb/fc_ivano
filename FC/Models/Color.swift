//
//  Color.swift
//  FC
//
//  Created by Fausto Savatteri on 25/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import ObjectMapper

class Color: Mappable {
    
    var colorId: Int?
    var colorName: String? = ""
    var colorRGB: String? = ""
    
    
    required convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        colorId                        <- map["id"]
        colorName                    <- map["name"]
        colorRGB                    <- map["rgb"]
    }

}
