//
//  User.swift
//  FC
//
//  Created by Fausto Savatteri on 06/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import ObjectMapper

class Body: Mappable{
    var errors : String = ""
    var data : User?
       
       
    required convenience init?(map: Map) {
           self.init()
           self.mapping(map: map)
    }
       
       
    func mapping(map: Map) {
           errors                          <- map["errors"]
           data                          <- map["data"]
    }

}

class User: Mappable {
    
    var PHPSESSID : String = ""
    var status : String = ""
    var userdata : UserData?
    
    
    required convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        PHPSESSID                          <- map["PHPSESSID"]
        status                          <- map["status"]
        userdata                          <- map["userdata"]


    }

}


class UserData: Mappable {
    
    var id : String = ""
    var email : String = ""
    var firstname : String = ""
    var lastname : String = ""
    var language_code : String = ""
    var nickname : String = ""
    var username : String = ""
    
    
    required convenience init?(map: Map) {
        self.init()
        self.mapping(map: map)
    }
    
    
    func mapping(map: Map) {
        id                          <- map["id"]
        email                          <- map["email"]
        firstname                          <- map["firstname"]
        lastname                          <- map["lastname"]
        language_code                          <- map["language_code"]
        username                          <- map["username"]
        nickname                          <- map["nickname"]

    }

}
