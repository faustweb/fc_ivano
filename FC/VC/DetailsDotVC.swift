//
//  DetailsDotVC.swift
//  FC
//
//  Created by Fausto Savatteri on 05/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//
import UIKit
import FittedSheets


class DetailsDotVC: UIViewController {
    
    @IBOutlet weak var dsf: UILabel!
    
    var url = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Sono dentro Details Dot VC")
        // Do any additional setup after loading the view.
        
        self.dsf.text = FCSDK.dotSelezionato?.description
        self.url = FCSDK.dotSelezionato?.url ?? "http://www.festivalle.it"
       
        //self.url = FCSDK.urlDotSelezionato ?? "http://www.festivalle.it"
        
        /*for dot in FCSDK.dotsArrayInCellView {
            if dot.id == FCSDK.dotIdSelezionato {
                self.dsf.text = dot.description
                self.url = dot.url ?? "non c'è url"
            }
        }*/
        
        /*//dsf.text = FCSDK.nomeDotSelezionato
        if FCSDK.nomeDotSelezionato != "" {
            dsf.text = FCSDK.nomeDotSelezionato
        }
        else {
            dsf.text = "DESCRIZIONE NON PRESENTE"
        }*/
        
        

    }
    
    
    //mettere url 
    @IBAction func goToWebsite(_ sender: Any) {
        if let url = URL(string: self.url) {
                   UIApplication.shared.open(url)
               }
    }
    
    
    
}

   
