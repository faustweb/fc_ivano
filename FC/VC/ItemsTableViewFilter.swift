//
//  ItemsTableViewFilter.swift
//  Fashioncratic
//
//  Created by Fausto Savatteri on 26/06/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire


class ItemsTableViewFilter: UIViewController, UICollectionViewDelegate {

    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var searchController: UISearchBar!
    
    @IBOutlet weak var labelResults: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    
    @IBOutlet weak var clearButtonResults: UIButton!
    
    var itemsArray = ["PANTALONI", "SPORTWEAR","T-SHIRT", "TOP", "TUTE", "SALOPETTE", "SCARPE", "STIVALI", "CAPPELLI", "UNDERWEAR"]
    
    var filtroArray = ["ITEM","BRAND", "COUNTRY", "CIRCLE"]
    var filtroIndex = 0
    var isTagged = false

    
    @IBOutlet weak var collectionViewA: UICollectionView!

    @IBOutlet weak var collectionViewB: UICollectionView!
    
    
    let collectionViewAIdentifier = "CollectionViewACell"
    let collectionViewBIdentifier = "CollectionViewBCell"

    var data : DataSDK? = nil
    var dataSlug = ""
    var item : Item? = nil
    var color : Color? = nil
    
    var post : Post? = nil
    
    var cellsInfoColorArray = [[String : Any]]()
    
    var cellIsAlreadySelected = false
    var selectedIndexPath: IndexPath? = nil
    
    var filterNumbers = 0
    
    @IBAction func clearButtonAction(_ sender: Any) {

        
        labelResults.text = "ALL"
        FCSDK.filterLabelSelection = "ALL"
        
        FCSDK.filterIsSelected = false
        
        self.tableView.deselectSelectedRow(animated: true)
        
        FCSDK.colorIdToSend = 0
        
        label2.alpha = 0
        label3.alpha = 0
        label4.alpha = 0
    }
    
    @IBOutlet weak var noColorButton: UIButton!
    @IBAction func noColorButtonAction(_ sender: Any) {
        blackButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        labelResults.text = "\(self.labelResults.text ?? "") NO COLOR"
        FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) NO COLOR"
        
        FCSDK.colorIdToSend = 0
    }
    @IBOutlet weak var blackButton: UIButton!
    @IBAction func blackButtonAction(_ sender: Any) {
        noColorButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        labelResults.text = "\(self.labelResults.text ?? "") BLACK"
        FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) BLACK"

        
        FCSDK.colorIdToSend = 1


    }
    @IBOutlet weak var greyButton: UIButton!
    @IBAction func greyButtonAction(_ sender: Any) {
        noColorButton.isEnabled = false
        blackButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        labelResults.text = "\(self.labelResults.text ?? "") GREY"
        FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) GREY"
        
        FCSDK.colorIdToSend = 2


    }
    @IBOutlet weak var purpleButton: UIButton!
    @IBAction func purpleButtonAction(_ sender: Any) {
        noColorButton.isEnabled = false
        greyButton.isEnabled = false
        blackButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        labelResults.text = "\(self.labelResults.text ?? "") PURPLE"
        FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) PURPLE"

        FCSDK.colorIdToSend = 3


    }
    @IBOutlet weak var darkBlueButton: UIButton!
    @IBAction func darkBlueButtonAction(_ sender: Any) {
        noColorButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        blackButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        labelResults.text = "\(self.labelResults.text ?? "") BLUE NAVY"
        FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) BLUE NAVY"
        
        FCSDK.colorIdToSend = 4


    }
    @IBOutlet weak var lightBlueButton: UIButton!
    @IBAction func lightBlueButtonAction(_ sender: Any) {
        noColorButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        blackButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        labelResults.text = "\(self.labelResults.text ?? "") LIGHT BLUE"
        FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) LIGHT BLUE"
        
        FCSDK.colorIdToSend = 5


    }
    @IBOutlet weak var greenButton: UIButton!
    @IBAction func greenButtonAction(_ sender: Any) {
        noColorButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        blackButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        labelResults.text = "\(self.labelResults.text ?? "") GREEN"
        FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) GREEN"

        FCSDK.colorIdToSend = 6

    }
    @IBOutlet weak var redButton: UIButton!
    @IBAction func redButtonAction(_ sender: Any) {
        noColorButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        blackButton.isEnabled = false
        orangeButton.isEnabled = false
        whiteButton.isEnabled = false
        
        labelResults.text = "\(self.labelResults.text ?? "") RED"
        FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) RED"
        
        FCSDK.colorIdToSend = 7


    }
    @IBOutlet weak var orangeButton: UIButton!
    @IBAction func orangeButtonAction(_ sender: Any) {
        noColorButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        blackButton.isEnabled = false
        whiteButton.isEnabled = false
        
        labelResults.text = "\(self.labelResults.text ?? "") ORANGE"
        FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) ORANGE"
        
        FCSDK.colorIdToSend = 8


    }
    @IBOutlet weak var whiteButton: UIButton!
    @IBAction func whiteButtonAction(_ sender: Any) {
        noColorButton.isEnabled = false
        greyButton.isEnabled = false
        purpleButton.isEnabled = false
        darkBlueButton.isEnabled = false
        lightBlueButton.isEnabled = false
        greenButton.isEnabled = false
        redButton.isEnabled = false
        orangeButton.isEnabled = false
        blackButton.isEnabled = false
        
        labelResults.text = "\(self.labelResults.text ?? "") WHITE"
        FCSDK.filterLabelSelection = "\(FCSDK.filterLabelSelection) WHITE"
        
        FCSDK.colorIdToSend = 9


    }
    
    @IBOutlet weak var FCButton: UIButton!
    @IBAction func FCButtonAction(_ sender: Any) {
                
        if self.labelResults.text == "ALL" {
            FCSDK.filterNumbers = 0

            FCSDK.stringToSendForFilter = "manage-posts/posts"
            FCSDK.filterIsSelected = false
        }
        else {
            FCSDK.filterNumbers = self.filterNumbers

            FCSDK.stringToSendForFilter = "manage-posts/posts?itemid=\(FCSDK.itemIdToSend)&colorid=\(FCSDK.colorIdToSend)&brandid=\(FCSDK.brandIdToSend)&circleid=\(FCSDK.circleIdToSend)&countryid=\(FCSDK.countryIdToSend)"
            FCSDK.filterIsSelected = true
        }
        
        getPostFilteredCall(success:{
            print("invoco con successo getPost")
            
            
            //Test nuovo metodo
            print("il post array contiene \(FCSDK.postArray.count) elementi")

                
                //visualizzo ITEMS con filtro applicato
            let firstVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "goToFirstVC") as? FirstViewController
            firstVC?.modalPresentationStyle = .fullScreen
            self.present(firstVC!, animated: true, completion: nil)
            
            
        }, failure: { responseData in
            print(responseData)
        })
    }

    //MARK: UPDATE --> set le nuove label per filtri ricerca con bordo
    func setLabelsStyle() {
        labelResults?.layer.cornerRadius = (labelResults?.frame.size.height)!/1.8
        labelResults?.layer.masksToBounds = true
        labelResults?.layer.borderWidth = 1.3
        //labelResults.adjustsFontSizeToFitWidth = true
        //labelResults.minimumScaleFactor = 0.5
        
        label2?.layer.cornerRadius = (label2?.frame.size.height)!/2.0
        label2?.layer.masksToBounds = true
        label2?.layer.borderWidth = 1.3
        //label2.adjustsFontSizeToFitWidth = true
        //label2.minimumScaleFactor = 0.5
        label2.alpha = 0
        
        label3?.layer.cornerRadius = (label3?.frame.size.height)!/2.0
        label3?.layer.masksToBounds = true
        label3?.layer.borderWidth = 1.3
        //label3.adjustsFontSizeToFitWidth = true
        //label3.minimumScaleFactor = 0.5
        label3.alpha = 0

        label4?.layer.cornerRadius = (label4?.frame.size.height)!/2.0
        label4?.layer.masksToBounds = true
        label4?.layer.borderWidth = 1.3
        //label4.adjustsFontSizeToFitWidth = true
        //label4.minimumScaleFactor = 0.5
        label4.alpha = 0

    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setLabelsStyle()
        
        
        if FCSDK.filterIsSelected == false {
            labelResults.text = "ALL"
            FCSDK.filterLabelSelection = "ALL"
        }
        
        else if FCSDK.filterIsSelected == true && FCSDK.filterLabelSelection != "ALL"{
        //è true, filtro attivo, visualizza le ricerche salvate in precedenza
            
            if FCSDK.filterNumbers == 1 {
                labelResults.text = FCSDK.filterLabelSelection
            }
            else if FCSDK.filterNumbers == 2 {
                label2.alpha = 1
                labelResults.text = FCSDK.filterLabelSelection
                label2.text = FCSDK.filterLabel2
            }
            else if FCSDK.filterNumbers == 3 {
                label2.alpha = 1
                label3.alpha = 1
                labelResults.text = FCSDK.filterLabelSelection
                label2.text = FCSDK.filterLabel2
                label3.text = FCSDK.filterLabel3

            }
            else if FCSDK.filterNumbers == 4 {
                label2.alpha = 1
                label3.alpha = 1
                label4.alpha = 1
                labelResults.text = FCSDK.filterLabelSelection
                label2.text = FCSDK.filterLabel2
                label3.text = FCSDK.filterLabel3
                label4.text = FCSDK.filterLabel4

            }
            
          
        }
        
        labelResults.text = FCSDK.filterLabelSelection

        
        print("il circleArray quanti elementi ha?")
        print(FCSDK.circleArray.count)
        
        tableView.delegate = self
        tableView.dataSource = self
                
        collectionViewB.delegate = self

        collectionViewB.dataSource = self

        self.view.addSubview(collectionViewB)

        print("la dimensione di colorArray usato come test è:")
        print(FCSDK.colorArray.count)
    

    }
    
    override func viewWillAppear(_ animated: Bool) {

            super.viewWillAppear(animated)
            self.tableView.reloadData()
        
        if FCSDK.filterIsSelected == true && FCSDK.filterLabelSelection != "ALL"{ //è true, significa che voglio filtrare la seconda volta, faccio vedere cosa avevo cercato prima
            labelResults.text = FCSDK.filterLabelSelection
            
            //self.collectionViewB.selectItem(at: FCSDK.daColorareAIndex, animated: true, scrollPosition: UICollectionView.ScrollPosition(rawValue: 0))
            self.collectionViewB.selectItem(at: FCSDK.daColorareAIndex, animated: true, scrollPosition: .top)
            self.tableView.selectRow(at: FCSDK.indexDaColorare, animated: true, scrollPosition: .middle)
            
          

        }
            
    
      }
    
    
}
 

extension ItemsTableViewFilter: UITableViewDelegate {
    

    
  
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("riga selezionata")
        
        
        let cell = tableView.cellForRow(at: indexPath) as! ItemsTableViewCell
        

        self.cellIsAlreadySelected = true
        

        
        if filtroIndex == 0 {
            //ITEMS
            if selectedIndexPath == indexPath && FCSDK.filterIsSelected == true ||  FCSDK.indexDaColorare == indexPath && FCSDK.filterIsSelected == true {
                        // it was already selected
                print(cell.item.text!)
                print("era già selezionato, quindi rimuovo")

                        selectedIndexPath = nil
                        tableView.deselectRow(at: indexPath, animated: false)
                        labelResults.text = "ALL"
                        FCSDK.filterLabelSelection = "ALL"
                        FCSDK.itemIdToSend = 0
                        FCSDK.brandIdToSend = 0
                        FCSDK.countryIdToSend = 0
                        FCSDK.circleIdToSend = 0
                
                        FCSDK.filterIsSelected = false //filtro disattivo se deseleziono
                        isTagged = false
                
                        label2.alpha = 0
                        label3.alpha = 0
                        label4.alpha = 0

            }
            
            else {      // wasn't yet selected, so let's remember it
                FCSDK.indexDaColorare = indexPath
                selectedIndexPath = indexPath
                //MARK: filtro risultati selezionati, assegna valore alla label
                if labelResults.text != "ALL" {
                    
                    if self.filterNumbers == 1 {
                        label2.alpha = 1
                        label2.text = cell.item.text
                        FCSDK.filterLabel2 = cell.item.text!
                        self.filterNumbers = 2
                    }
                    
                    else if self.filterNumbers == 2 {
                        label3.alpha = 1
                        label3.text = cell.item.text
                        FCSDK.filterLabel3 = cell.item.text!
                        self.filterNumbers = 3
                    }
                    
                    else if self.filterNumbers == 3 {
                        label4.alpha = 1
                        label4.text = cell.item.text
                        FCSDK.filterLabel4 = cell.item.text!
                        self.filterNumbers = 4
                    }
                    
                    else if self.filterNumbers == 4 {
                        labelResults.text = "ALL"
                        label2.text = ""
                        label3.text = ""
                        label4.text = ""
                        label2.alpha = 0
                        label3.alpha = 0
                        label4.alpha = 0
                        FCSDK.filterLabel4 = ""
                        FCSDK.filterLabel2 = ""
                        FCSDK.filterLabel3 = ""
                        self.filterNumbers = 0
                    }
                    
                    

                }
                else {
                    print("primo risultato nella label1")
                    labelResults.text = cell.item.text
                    FCSDK.filterLabelSelection = (cell.item.text!)
                    self.filterNumbers = 1
                }
                FCSDK.filterIsSelected = true
                FCSDK.itemIdToSend = FCSDK.itemsArray[indexPath.row].itemId
                isTagged = true
            }
            
            
        }
        else if filtroIndex ==  1 { //BRAND
            
            
            if selectedIndexPath == indexPath ||  FCSDK.indexDaColorare == indexPath && FCSDK.filterIsSelected == true {
                        // it was already selected
                        print(cell.item.text!)
                        print("era già selezionato, quindi rimuovo")
                        selectedIndexPath = nil
                        tableView.deselectRow(at: indexPath, animated: false)
                        labelResults.text = "ALL"
                        FCSDK.filterLabelSelection = "ALL"
                        FCSDK.itemIdToSend = 0
                        FCSDK.brandIdToSend = 0
                        FCSDK.countryIdToSend = 0
                        FCSDK.circleIdToSend = 0
                
                        FCSDK.filterIsSelected = false //filtro disattivo se deseleziono
                        isTagged = false
            }
            
            else {      // wasn't yet selected, so let's remember it
                FCSDK.indexDaColorare = indexPath
                selectedIndexPath = indexPath
                //MARK: filtro risultati selezionati, assegna valore alla label
                if labelResults.text != "ALL" {
                    
                    if self.filterNumbers == 1 {
                        label2.alpha = 1
                        label2.text = cell.item.text
                        FCSDK.filterLabel2 = cell.item.text!
                        self.filterNumbers = 2
                    }
                    
                    else if self.filterNumbers == 2 {
                        label3.alpha = 1
                        label3.text = cell.item.text
                        FCSDK.filterLabel3 = cell.item.text!
                        self.filterNumbers = 3
                    }
                    
                    else if self.filterNumbers == 3 {
                        label4.alpha = 1
                        label4.text = cell.item.text
                        FCSDK.filterLabel4 = cell.item.text!
                        self.filterNumbers = 4
                    }
                    else if self.filterNumbers == 4 {
                        labelResults.text = "ALL"
                        label2.text = ""
                        label3.text = ""
                        label4.text = ""
                        label2.alpha = 0
                        label3.alpha = 0
                        label4.alpha = 0
                        FCSDK.filterLabel4 = ""
                        FCSDK.filterLabel2 = ""
                        FCSDK.filterLabel3 = ""
                        self.filterNumbers = 0
                    }
                    

                }
                else {
                    print("primo risultato nella label1")
                    labelResults.text = cell.item.text
                    FCSDK.filterLabelSelection = (cell.item.text!)
                    self.filterNumbers = 1

                }
                FCSDK.filterIsSelected = true
                FCSDK.brandIdToSend = FCSDK.brandsArray[indexPath.row].brandId ?? 0
                isTagged = true


            }

        }
        else if filtroIndex ==  2 { //COUNTRY
            
            if selectedIndexPath == indexPath ||  FCSDK.indexDaColorare == indexPath && FCSDK.filterIsSelected == true {
                        // it was already selected
                        print(cell.item.text!)
                        print("era già selezionato, quindi rimuovo")
                        selectedIndexPath = nil
                        tableView.deselectRow(at: indexPath, animated: false)
                        labelResults.text = "ALL"
                        FCSDK.filterLabelSelection = "ALL"
                        FCSDK.itemIdToSend = 0
                        FCSDK.brandIdToSend = 0
                        FCSDK.countryIdToSend = 0
                        FCSDK.circleIdToSend = 0
                
                        FCSDK.filterIsSelected = false //filtro disattivo se deseleziono
                        isTagged = false

            }
            
            else {      // wasn't yet selected, so let's remember it
                FCSDK.indexDaColorare = indexPath
                selectedIndexPath = indexPath
                //MARK: filtro risultati selezionati, assegna valore alla label
                if labelResults.text != "ALL" {
                    
                    if self.filterNumbers == 1 {
                        label2.alpha = 1
                        label2.text = cell.item.text
                        FCSDK.filterLabel2 = cell.item.text!
                        self.filterNumbers = 2
                    }
                    
                    else if self.filterNumbers == 2 {
                        label3.alpha = 1
                        label3.text = cell.item.text
                        FCSDK.filterLabel3 = cell.item.text!
                        self.filterNumbers = 3
                    }
                    
                    else if self.filterNumbers == 3 {
                        label4.alpha = 1
                        label4.text = cell.item.text
                        FCSDK.filterLabel4 = cell.item.text!
                        self.filterNumbers = 4
                    }
                    else if self.filterNumbers == 4 {
                        labelResults.text = "ALL"
                        label2.text = ""
                        label3.text = ""
                        label4.text = ""
                        label2.alpha = 0
                        label3.alpha = 0
                        label4.alpha = 0
                        FCSDK.filterLabel4 = ""
                        FCSDK.filterLabel2 = ""
                        FCSDK.filterLabel3 = ""
                        self.filterNumbers = 0
                    }
                    

                }
                else {
                    print("primo risultato nella label1")
                    labelResults.text = cell.item.text
                    FCSDK.filterLabelSelection = (cell.item.text!)
                    self.filterNumbers = 1

                }
                
                FCSDK.filterIsSelected = true
                FCSDK.countryIdToSend = FCSDK.countryArray[indexPath.row].countryId
                isTagged = true


            }

        }
        else if filtroIndex ==  3 { //CIRCLE

            
            if selectedIndexPath == indexPath ||  FCSDK.indexDaColorare == indexPath && FCSDK.filterIsSelected == true {
                        // it was already selected
                        print(cell.item.text!)
                        print("era già selezionato, quindi rimuovo")
                        selectedIndexPath = nil
                        tableView.deselectRow(at: indexPath, animated: false)
                        labelResults.text = "ALL"
                        FCSDK.filterLabelSelection = "ALL"
                        FCSDK.itemIdToSend = 0
                        FCSDK.brandIdToSend = 0
                        FCSDK.countryIdToSend = 0
                        FCSDK.circleIdToSend = 0
                        FCSDK.filterIsSelected = false //filtro disattivo se deseleziono
                        isTagged = false

            }
            
            else {      // wasn't yet selected, so let's remember it
                FCSDK.indexDaColorare = indexPath
                selectedIndexPath = indexPath
                //MARK: filtro risultati selezionati, assegna valore alla label
                if labelResults.text != "ALL" {
                    
                    if self.filterNumbers == 1 {
                        label2.alpha = 1
                        label2.text = cell.item.text
                        FCSDK.filterLabel2 += cell.item.text!
                        self.filterNumbers = 2
                    }
                    
                    else if self.filterNumbers == 2 {
                        label3.alpha = 1
                        label3.text = cell.item.text
                        FCSDK.filterLabel3 += cell.item.text!
                        self.filterNumbers = 3
                    }
                    
                    else if self.filterNumbers == 3 {
                        label4.alpha = 1
                        label4.text = cell.item.text
                        FCSDK.filterLabel4 += cell.item.text!
                        self.filterNumbers = 4
                    }
                    else if self.filterNumbers == 4 {
                        labelResults.text = "ALL"
                        label2.text = ""
                        label3.text = ""
                        label4.text = ""
                        label2.alpha = 0
                        label3.alpha = 0
                        label4.alpha = 0
                        FCSDK.filterLabel4 = ""
                        FCSDK.filterLabel2 = ""
                        FCSDK.filterLabel3 = ""
                        self.filterNumbers = 0
                    }
                    

                }
                else {
                    print("primo risultato nella label1")
                    labelResults.text = cell.item.text
                    FCSDK.filterLabelSelection = (cell.item.text!)
                    self.filterNumbers = 1

                }
                FCSDK.filterIsSelected = true
                FCSDK.circleIdToSend = FCSDK.circleArray[indexPath.row].circleId
                isTagged = true


            }
        }
            
            //MARK: UPDATE, lo invoco con pressione FC
            //chiama post filtrato con immagine di esempio e poi chiama post set vote
            
                      
        }
    //}
}

extension ItemsTableViewFilter: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if filtroIndex == 0 {
                //ITEMS
                return FCSDK.itemsArray.count
            }
            else if filtroIndex ==  1 { //BRAND
                return FCSDK.brandsArray.count

            }
            else if filtroIndex ==  2 { //COUNTRY
                return FCSDK.countryArray.count
            }
            else if filtroIndex ==  3 { //CIRCLE
                return FCSDK.circleArray.count
            }
            return FCSDK.itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ItemsTableViewCell
        
        let backgroundView = UIView()
        let customColor = UIColor(hexString: "#d4af37")  //uso mia extension per esprimere colore in hex
        backgroundView.backgroundColor = customColor
        cell.selectedBackgroundView = backgroundView
        
        
        
        if filtroIndex == 0 {
            //ITEMS
            cell.item.text = FCSDK.itemsArray[indexPath.row].itemName
        }
        else if filtroIndex ==  1 { //BRAND
            cell.item.text = FCSDK.brandsArray[indexPath.row].brandName
            
        }
        else if filtroIndex ==  2 { //COUNTRY
            cell.item.text = FCSDK.countryArray[indexPath.row].countryName
        }
        else if filtroIndex ==  3 { //CIRCLE
            cell.item.text = FCSDK.circleArray[indexPath.row].circleName
        }

        
        
        return cell
    }
    

}

extension ItemsTableViewFilter: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionViewA {
            //return brandArray.count
            return FCSDK.colorArray.count
        }
        else {
            return filtroArray.count
            //return FCSDK.itemsArray.count

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionViewA {
            let cellA = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! ItemsCollectionViewCell  //CollectionViewCell è la mia custom class per definire la cella
            
            

            return cellA
        }

        else {
            let cellB = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell2", for: indexPath) as! ItemsCollectionViewCell  //CollectionViewCell è la mia custom class per definire la cella

            cellB.category.text = self.filtroArray[indexPath.item]
            //cellB.category.text = FCSDK.itemsArray[indexPath.item].itemName
            
            //cambia table view in base a filtro
            

            return cellB
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
            didSelectItemAt indexPath: IndexPath) {
                //let photo = (indexPath)
                let cell = collectionView.cellForItem(at: indexPath)

            
        if (indexPath.item == 0) {
                    print("Ho selezionato la prima cella: ITEM, sto già mostrando item su tableview")
                    if self.filtroIndex != 0 {
                        self.filtroIndex = 0
                        FCSDK.daColorareAIndex = indexPath
                        self.tableView.reloadInputViews()
                        self.tableView.reloadData()
                    }
                }

                else if (indexPath.item == 1) {
                    print("Ho selezionato la seconda cella BRAND: mostro brand in table view")
                    self.filtroIndex = 1
                    FCSDK.daColorareAIndex = indexPath
                    self.tableView.reloadInputViews()
                    self.tableView.reloadData()
                }
                else if (indexPath.item == 2) {
                    print("Ho selezionato la terza cella")
                    self.filtroIndex = 2
                    FCSDK.daColorareAIndex = indexPath
                    self.tableView.reloadInputViews()
                    self.tableView.reloadData()
                }
                else if (indexPath.item == 3) {
                    print("Ho selezionato la quarta cella")
                    self.filtroIndex = 3
                    FCSDK.daColorareAIndex = indexPath
                    self.tableView.reloadInputViews()
                    self.tableView.reloadData()
                }
        }
    
    // MARK: UICollectionViewDelegate
    
    
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        //var cell = collectionView.cellForItemAtIndexPath(indexPath)
        //cell.backgroundColor = UIColor.redColor()
       // cell.overlayView.backgroundColor = UIColor.clearColor()

    return true
    }
    
    // Uncomment this method to specify if the specified item should be selected
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        
    return true
    }
}



extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension UITableView {

    func deselectSelectedRow(animated: Bool)
    {
        if let indexPathForSelectedRow = self.indexPathForSelectedRow {
            self.deselectRow(at: indexPathForSelectedRow, animated: animated)
        }
    }

}
