//
//  TableViewSwipeUpFirstVC.swift
//  FC
//
//  Created by Fausto Savatteri on 29/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire
import Kingfisher

extension FirstViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        menuView.isHidden = true

        hideApplausometro()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FCSDK.postArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TableViewSwipeCustomCell
        cell.viewController = self
        
        setPhoto(row: indexPath.row, photo: cell.photo)
        FCSDK.photoIdToVote = FCSDK.postArray[indexPath.row].id

        if showingDots {
            showDots(cell, indexPath.row)
            showLabelTitle(cell, indexPath.row)
        } else {
            hideDots(cell)
            hideLabelTitle(cell)
        }
        
        self.indexSoloPath = indexPath
        self.indexPerDot = indexPath.row
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if slider.isHidden == false {
            hideApplausometro()
        } else {
            showingDots = !showingDots
            
            let contentOffset = tableView.contentOffset
            self.tableView.beginUpdates()
            self.tableView.reloadRows(at: [indexPath], with: .none)
            self.tableView.endUpdates()
            self.tableView.layer.removeAllAnimations()
            self.tableView.setContentOffset(contentOffset, animated: false)
        }
    }
    
    
    func setPhoto(row: Int, photo: UIImageView) {
        let imageUrl = FCSDK.postArray[row].urlImg
        let url = URL(string: imageUrl)
        photo.kf.setImage(with: url)
    }
    
    
    func showDots(_ cell: TableViewSwipeCustomCell, _ row: Int) {
            cell.setDot(dotArray: FCSDK.postArray[row].dots!, view: cell.photo)
            
            FCSDK.dotsArrayInCellView.removeAll()
            for dot in FCSDK.postArray[row].dots! {
                FCSDK.dotsArrayInCellView.append(dot)
            }
    }
    
    
    func hideDots(_ cell: TableViewSwipeCustomCell?) {
        cell!.photo.clearSubviews()
    }
    
    
    func showLabelTitle(_ cell: TableViewSwipeCustomCell, _ row: Int) {
        cell.labelTitle.text = FCSDK.postArray[row].caption
        let posY = CGFloat(FCSDK.postArray[row].captionPosizioneAsseY!)
        let posYreale = posY * self.altezzaSchermo!
        
        cell.labelTitleTopConstraint.constant = posYreale
        cell.labelTitle.isHidden = false
    }
    
    
    func hideLabelTitle(_ cell: TableViewSwipeCustomCell) {
        cell.labelTitle.isHidden = true
    }
    
    
    func hideApplausometro() {
        slider.isHidden = true
        applausometroSliderBackground.isHidden = true
        outletApplausometro.isHidden = false
    }
    
    
    func showApplausometro() {
        slider.isHidden = false
        applausometroSliderBackground.isHidden = false
        outletApplausometro.isHidden = true
    }
}


