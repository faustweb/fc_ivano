//
//  PhotoAlbumVC.swift
//  FC
//
//  Created by Fausto Savatteri on 30/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//


import UIKit

class PhotoViewController: UIViewController {
     @IBOutlet var imageView: UIImageView!
       
       var image:UIImage?
       
       override func viewDidLoad() {
           super.viewDidLoad()

           imageView.image = image
       }

       override func didReceiveMemoryWarning() {
           super.didReceiveMemoryWarning()
           // Dispose of any resources that can be recreated.
       }
       

       // MARK: - Action methods
       
       @IBAction func save(sender: UIButton) {
       }
}
