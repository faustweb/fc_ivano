//
//  SearchVC.swift
//  FC
//
//  Created by Fausto Savatteri on 29/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//


import Foundation
import UIKit
import FittedSheets
import BonMot
import Alamofire


class SearchVC: UIViewController, UICollectionViewDelegate {

    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var suggestedNamesArray = ["CANDELA PELIZZA", "NOME 2","NOME 3", "NOME 4", "NOME 5"]
    //var suggestedPhotoArray = ["candela.jpg", "foto2.jpg","foto3.jpg", "foto4.jpg", "foto5.jpg"]
    var suggestedPhotoArray = ["oval.png", "oval.png","oval.png", "oval.png", "oval.png"]

    //var brandArray = ["ARMANI", "D&G","FENDI", "GUCCI", "ROSSETTI", "ZEGNA"]
    var filtroArray = ["ITEM", "SORT BY","BRAND", "COUNTRY", "CIRCLE"]
    var filtroIndex = 0

    @IBAction func homeButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "goToFirstVC") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
    }

    @IBAction func backButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyboard.instantiateViewController(withIdentifier: "goToFirstVC") // as! UIViewController
        secondViewController.modalPresentationStyle = .fullScreen
        self.present(secondViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.searchBar.resignFirstResponder()

    

    }
    
    
    
}
