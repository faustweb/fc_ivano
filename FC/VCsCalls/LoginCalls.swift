//
//  LoginCalls.swift
//  FC
//
//  Created by Fausto Savatteri on 06/10/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import UIKit
import SwiftKeychainWrapper


extension LoginViewController {
    
    func postLoginCall(username: String, nickname: String, success:@escaping ()->(), failure:@escaping (String)->()) {
        postLogin(username: username, nickname: nickname, success: { responseData in
            do {
                let jsonResult = try JSONSerialization.jsonObject(with: responseData, options:
                                                                    JSONSerialization.ReadingOptions.mutableContainers)
                FCSDK.userArray = []
                
                if let jsonResultDictionary = jsonResult as? [String : Any],
                   let body = jsonResultDictionary["body"] as? [String : Any],
                   let item = body["data"] as? [String : Any],
                   let mappedElement = Mapper<User>().map(JSON: item) {
                    FCSDK.userArray.append(mappedElement)
                    
                    success()
                } else {
                    failure(FCSDK.handleFailure(json: nil))
                }
            } catch {
                failure(FCSDK.handleFailure(json: nil))
            }
        }, failure: { responseData in
            failure(FCSDK.handleAlamofireError(responseData: responseData))
        })
    }
    
    
    func postLogin(username: String, nickname: String, success:@escaping (Data)->(), failure:@escaping (Any?)->()) {
        
        let parameters: [String:Any] = ["username": username, "nickname": nickname]
        
        AF.request(FCSDK.rootUrl + "auth/login", method: .post, parameters: parameters, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseData { response in
            
            switch response.result {
            case .success:
                //if Utils.pickRandomBool(0.9){
                if response.error == nil {
                    success(response.data!)
                    debugPrint("Response: \(response)")
                    
                } else {
                    failure(response.data!)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    
    
    
    
    
}

