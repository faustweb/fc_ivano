//
//  FirstVCCalls.swift
//  FC
//
//  Created by Fausto Savatteri on 25/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import ObjectMapper
import UIKit
import SwiftKeychainWrapper


extension LoginViewController {
    
    
    func getDataCall(success:@escaping ()->(), failure:@escaping (String)->()) {
        getData(success: { responseData in
            
            do {
                let jsonResult = try JSONSerialization.jsonObject(with: responseData, options:
                                                                    JSONSerialization.ReadingOptions.mutableContainers)
                FCSDK.dataArray = []
                FCSDK.itemsArray = []
                FCSDK.colorArray = []
                FCSDK.circleArray = []
                FCSDK.brandsArray = []
                
                if let jsonResultDictionary = jsonResult as? [String : Any],
                   let data = jsonResultDictionary["data"] as? [String : Any]{
                    if let item = data["item"] as? [[String : Any]] {
                        for elementItem in item {
                            if let mappedElement = Mapper<Item>().map(JSON: elementItem) {
                                FCSDK.itemsArray.append(mappedElement)
                            }
                        }
                    }
                    if let colors = data["color"] as? [[String : Any]] {
                        for colorItem in colors {
                            if let mappedElement = Mapper<Color>().map(JSON: colorItem) {
                                FCSDK.colorArray.append(mappedElement)
                                print(mappedElement)
                            }
                        }
                    }
                    if let circle = data["circle"] as? [[String : Any]] {
                        for circleItem in circle {
                            if let mappedElement = Mapper<Circle>().map(JSON: circleItem) {
                                FCSDK.circleArray.append(mappedElement)
                            }
                        }
                    }
                    if let brand = data["brand"] as? [[String : Any]] {
                        for brandItem in brand {
                            if let mappedElement = Mapper<Brand>().map(JSON: brandItem) {
                                FCSDK.brandsArray.append(mappedElement)
                            }
                        }
                    }
                    if let country = data["country"] as? [[String : Any]] {
                        for countryItem in country {
                            if let mappedElement = Mapper<Country>().map(JSON: countryItem) {
                                FCSDK.countryArray.append(mappedElement)
                            }
                        }
                    }
                    
                    success()
                } else {
                    failure(self.handleFailure(json: nil))
                }
            } catch {
                failure(self.handleFailure(json: nil))
            }
        }, failure: { responseData in
            failure(self.handleAlamofireError(responseData: responseData))
        })
    }
    
    
    func getData(success:@escaping (Data)->(), failure:@escaping (Any?)->()) {
        
        AF.request(FCSDK.rootUrl + "environment/data", method: .get, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseData { response in
            //Utils.hideLoading()
            
            switch response.result {
            case .success:
                //if Utils.pickRandomBool(0.9){
                if response.error == nil {
                    success(response.data!)
                } else {
                    failure(response.data!)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    
    func getPostCall(success:@escaping ()->(), failure:@escaping (String)->()) {
        getPost(success: { responseData in
            do {
                let jsonResult = try JSONSerialization.jsonObject(with: responseData, options:
                                                                    JSONSerialization.ReadingOptions.mutableContainers)
                FCSDK.postArray = []
                FCSDK.dotsArray = []
                
                
                if let jsonResultDictionary = jsonResult as? [String : Any],
                   let data = jsonResultDictionary["data"] as? [[String : Any]] {
                    for post in data {
                        if let mappedPost = Mapper<Post>().map(JSON: post) {
                            FCSDK.postArray.append(mappedPost)
                        }
                    }
                    success()
                } else {
                    failure(self.handleFailure(json: nil))
                }
            } catch {
                failure(self.handleFailure(json: nil))
            }
        }, failure: { responseData in
            failure(self.handleAlamofireError(responseData: responseData))
        })
    }
    
    func getPost(success:@escaping (Data)->(), failure:@escaping (Any?)->()) {
        
        AF.request(FCSDK.rootUrl + "manage-posts/posts", method: .get, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseData { response in
            //Utils.hideLoading()
            
            switch response.result {
            case .success:
                //if Utils.pickRandomBool(0.9){
                if response.error == nil {
                    success(response.data!)
                } else {
                    failure(response.data!)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
    
    
    func handleFailure(json: Any?) -> String {
        if let json = json as? [String: Any], let status = json["status"] as? String, status == "ko" {
            if let msgArray = json["msg"] as? NSArray {
                if let msg = msgArray[0] as? String {
                    return msg
                }
            }
        }
        return "error"
    }
    
    func handleAlamofireError(responseData: Any?) -> String {
        if let responseData = responseData as? Data {
            do {
                let jsonResult = try JSONSerialization.jsonObject(with: responseData, options:
                                                                    JSONSerialization.ReadingOptions.mutableContainers)
                return self.handleFailure(json: jsonResult)
            } catch {
                return self.handleFailure(json: nil)
            }
        } else {
            return self.handleFailure(json: nil)
        }
    }
    
}
