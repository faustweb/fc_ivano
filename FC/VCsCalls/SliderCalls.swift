//
//  SliderCalls.swift
//  FC
//
//  Created by Fausto Savatteri on 28/09/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.


import Foundation
import UIKit
import Alamofire
import ObjectMapper
import UIKit
import SwiftKeychainWrapper


extension FirstViewController {
    

    
    func postSetVote(postId: Int, vote: Int) {
          let urlRoot = URL(string: "\(FCSDK.rootUrl)manage-posts/set-vote")
                                                  guard let requestUrl = urlRoot else { fatalError() }
                                                  // Prepare URL Request Object
                                                  var request = URLRequest(url: requestUrl)
                                                  request.httpMethod = "POST"
                                                  print("Ho invocato il metodo post per Login")
                      
                                                  // HTTP Request Parameters which will be sent in HTTP Request Body
                                                  let postString = "postid=\(postId)&vote=\(vote)";
                                                  // Set HTTP Request Body
                                                  request.httpBody = postString.data(using: String.Encoding.utf8);
                                                  // Perform HTTP Request
                                                  let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                                                          
                                                          // Check for Error
                                                          if let error = error {
                                                              print("Error took place \(error)")
                                                              return
                                                          }
                                                   
                                                          // Convert HTTP Response Data to a String
                                                          if let data = data, let dataString = String(data: data, encoding: .utf8) {
                                                              print("Risposta da set Vote:\n \(dataString)")
                                                          }
                                                  }
                                                  task.resume()
      }
    
    
    
    func getPostFirstCall(success:@escaping ()->(), failure:@escaping (String)->()) {
          getPostFirst(success: { responseData in
           do {
                         let jsonResult = try JSONSerialization.jsonObject(with: responseData, options:
                                                JSONSerialization.ReadingOptions.mutableContainers)
                                            FCSDK.postArray = []
                                            FCSDK.dotsArray = []
                                         

                                            if let jsonResultDictionary = jsonResult as? [String : Any],
                                                let data = jsonResultDictionary["data"] as? [[String : Any]] {
                                                for post in data {
                                                    if let mappedPost = Mapper<Post>().map(JSON: post) {
                                                    FCSDK.postArray.append(mappedPost)
                                                        
                                            
                                                    }
                                                }
                                                success()
                                            } else {
                                                failure(self.handleFailure(json: nil))
                                            }
                                        } catch {
                                            failure(self.handleFailure(json: nil))
                                        }
                            }, failure: { responseData in
                                failure(self.handleAlamofireError(responseData: responseData))
                            })
      }
    
    func getPostFirst(success:@escaping (Data)->(), failure:@escaping (Any?)->()) {
        
        AF.request(FCSDK.rootUrl + "manage-posts/posts", method: .get, encoding: JSONEncoding.default).validate(statusCode: 200..<300).responseData { response in
                    //Utils.hideLoading()
                    
                    switch response.result {
                    case .success:
                        //if Utils.pickRandomBool(0.9){
                        if response.error == nil {
                            success(response.data!)
                        } else {
                            failure(response.data!)
                        }
                    case .failure(let error):
                        failure(error)
                    }
            }
    }
    
    
    func handleFailure(json: Any?) -> String {
        if let json = json as? [String: Any], let status = json["status"] as? String, status == "ko" {
            if let msgArray = json["msg"] as? NSArray {
                if let msg = msgArray[0] as? String {
                    return msg
                }
            }
        }
        return "error"
    }
    
    func handleAlamofireError(responseData: Any?) -> String {
        if let responseData = responseData as? Data {
            do {
                let jsonResult = try JSONSerialization.jsonObject(with: responseData, options:
                    JSONSerialization.ReadingOptions.mutableContainers)
                return self.handleFailure(json: jsonResult)
            } catch {
                return self.handleFailure(json: nil)
            }
        } else {
            return self.handleFailure(json: nil)
        }
    }
}
