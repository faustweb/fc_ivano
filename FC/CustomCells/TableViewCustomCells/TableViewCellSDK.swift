//
//  ItemTableViewCell.swift
//  Fashioncratic
//
//  Created by Fausto Savatteri on 26/06/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import Foundation
import UIKit
import BonMot


class ItemsTableViewCell: UITableViewCell {
        

    @IBOutlet weak var item: UILabel!
    
    @IBOutlet weak var category: UILabel!

    @IBOutlet weak var palette: UIColor!
    

    
    // ToDo Genere
    func setCell(_ item: String, category: String, palette: UIColor){
        self.item.text = item
        self.category.text = category
        self.palette = palette

    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
}

class ItemsCollectionViewCell: UICollectionViewCell {
        

    @IBOutlet weak var brandLabel: UILabel!
    
    @IBOutlet weak var category: UILabel!

    @IBOutlet weak var palette: UIColor!
    
    @IBOutlet weak var profileCirclePhoto: UIImageView!

    
    // ToDo Genere
    func setCell(_ brandLabel: String, category: String, palette: UIColor){
        self.brandLabel.text = brandLabel
        self.category.text = category
        self.palette = palette

    }
    
    
    func setProfilePhoto(foto: UIImage) {
        //if Is preferito è true la riga è preferita
        let image: UIImage = UIImage(named: "starSelected")!
        self.profileCirclePhoto.image = image
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
}


class CameraGalleryCollectionViewCell: UICollectionViewCell {
        

    @IBOutlet weak var galleryImage: UIImageView!
    @IBOutlet var cellView: UIView!
        
        
        /*func setStyle() {
            setView()
            setImage()
        }
        
        
        func setView() {
            cellView.backgroundColor = UIColor.white
            cellView.layer.cornerRadius = 8.0
            cellView.clipsToBounds = true
            //cellView.dropShadow()
            
            cellView.translatesAutoresizingMaskIntoConstraints = false
        }
        
        
    func setImage(image: UIImage) {
            image.kf.setImage(with: URL(string: "https://picsum.photos/seed/" + Utils.randomString(length: 6) + "/200/200"), placeholder: nil, options: [.transition(.fade(0.2))])
            
        }*/
        
        

    }


 
