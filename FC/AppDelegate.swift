//
//  AppDelegate.swift
//  FC
//
//  Created by Fausto Savatteri on 26/06/2020.
//  Copyright © 2020 Fausto Savatteri. All rights reserved.
//

import UIKit
import DropDown

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        //chiama post filtrato con immagine di esempio e poi chiama post set vote
        /*FCSDK.getPostCall(success:{
            print("invoco con successo getPost")
            
            //Test nuovo metodo
            print("il post array contiene \(FCSDK.postArray.count) elementi")
            
            
        }, failure: { responseData in
            print(responseData)
        })*/
        DropDown.startListeningToKeyboard()
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

